# On an 8x8 board, what is the minimum number of moves for a knight (horse) to get from any one spot to the next

TOP = (range(0, 8), (-17, -15, -10, -6))
TOP_SECONDARY = (range(8, 16), (-17, -15))
BOTTOM = (range(56, 64), (17, 15, 10, 6))
BOTTOM_SECONDARY = (range(48, 56), (17, 15))

LEFT = (range(0, 57, 8), (-10, -17, 6, 15))
LEFT_SECONDARY = (range(1, 58, 8), (6, -10))
RIGHT = (range(7, 64, 8), (-15, -6, 10, 17))
RIGHT_SECONDARY = (range(6, 63, 8), (-6, 10))

NEXT_STEP_CALCULATION = [-17, -15, -10, -6, 6, 10, 15, 17]


def next_step_location_generator(number):
    calculation = list(NEXT_STEP_CALCULATION)

    for location in [
        TOP, TOP_SECONDARY, BOTTOM, BOTTOM_SECONDARY,
        LEFT, LEFT_SECONDARY, RIGHT, RIGHT_SECONDARY,
    ]:
        if number in location[0]:
            for value in location[1]:
                if value in calculation:
                    calculation.remove(value)

    return calculation


SQUARES = [(number, [number + value for value in next_step_location_generator(number)]) for number in range(64)]


def solution(src, dest):
    looking = set([src])
    to_investigate = set()
    investigated = set()
    step = 0

    while dest not in looking:

        step += 1

        to_investigate.update(looking)
        looking.clear()

        for square in to_investigate:
            for child in SQUARES[square][1]:
                looking.add(child)

        investigated.update(to_investigate)
        to_investigate.clear()

    return step



if __name__ == "__main__":
    assert solution(0, 1) == 3
    assert solution(19, 36) == 1
    assert solution(0, 63) == 6
    assert solution(40, 47) == 5
    assert (solution(56, 63)) == 5

